from bot.view_helpers import AugementedView, find_books
from message_parser.main import extract_message_fields

import json
from django.http import JsonResponse

from bot.setup import GROUP_CHAT_ID, BOT_NAME

ALLOWED_CHATS = [GROUP_CHAT_ID]


class BotView(AugementedView):
    def post(self, request, *args, **kwargs):
        # store json received in a data dictionary
        received_data = json.loads(request.body)

        # extract the chat id where the message was received from
        chat_id = received_data['message']['chat']['id']

        # If the ID is not the CHRISTIAN_LIBRARY_CHAT_ID,
        # return sorry message with link to christian library group chat
        if (chat_id not in ALLOWED_CHATS):
            print("# DEBUG: Not Allowed")
            return_text = "I'm sorry.\n\n"
            return_text += "You are not allowed to chat with me here.\n"
            return_text += "I'd be glad to respond to you if you chat me up on the Ilanaa Demo group chat.\n"
            return_text += "You can use this link to join:\n\n"
            return_text += "https://t.me/joinchat/OpPw802Ulr_AcCtDxS-thw"

            self.send_response_message(chat_id, return_text)

            return JsonResponse({"ok": "Invalid Chat"})
        else:
            print("# DEBUG: Allowed")
            # retrieve message text
            # it's not every message that has text.
            # That is why the try/except is used to check if theres text
            message_details = {}
            message_details['chat_id'] = chat_id

            try:
                message_details['text'] = received_data['message']['text']
            except Exception as e:
                return JsonResponse({"ok": "No Text"})

            print("# DEBUG: Text Exists")

            request_fields = self.extract_text_fields(message_details['text'])

            print(request_fields)

            # only address a message if the bot_name can be extracted from it.
            # it it doesn't, ignore it. It wasn't directed at the bot
            if (("bot_name" in request_fields) and (request_fields["bot_name"] == BOT_NAME.lower())):
                print("# DEBUG: Bot Addressd")

                # store it in the database
                message_details['message_id'] = received_data['message']['message_id']
                message_details['sender_id'] = received_data['message']['from']['id']
                message_details['date'] = received_data['message']['date']

                return_text = "These are the fields present in your request:\n\n"
                return_text += self.generate_message_detail_string(request_fields)
                self.send_response_message(chat_id, return_text)

                books = find_books(request_fields)
                return_text = "These are the books matching your request:\n\n"
                for book in books:
                    return_text += self.generate_message_detail_string(book["fields"])
                # return_text += "Type: {type}".format(type=type(books))
                self.send_response_message(chat_id, return_text)

                self.save_message(message_details['chat_id'],
                                  message_details['message_id'],
                                  message_details['sender_id'],
                                  message_details['text'],
                                  message_details['date'])

                # return message as proof of receiving it
                # return_text = "Your message has been received\n"
                # return_text += self.generate_message_detail_string(message_details)

                # self.send_response_message(chat_id, return_text)

                return JsonResponse({"ok": "Message Received"})
            else:
                print("# DEBUG: Bot not Addressd")
                return JsonResponse({"ok": "Message Not Addressed to bot"})


"""elif (received_data['message']['from']['is_bot']):
    # don't send any reply so as not to crowd the room
    return JsonResponse({"ok": "Invalid Chat"})"""
