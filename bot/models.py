from django.db import models


# Create your models here.
class Message(models.Model):
    chat_id = models.BigIntegerField()
    message_id = models.IntegerField()
    sender_id = models.BigIntegerField()
    text = models.TextField()
    date = models.BigIntegerField()

    def __str__(self):
        return self.text
