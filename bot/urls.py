from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from bot.views import BotView

urlpatterns = [
    path('', csrf_exempt(BotView.as_view()), name='bot_endpoint'),
]
