from django.test import SimpleTestCase
from django.urls import reverse, resolve


# Create your tests here.
class TestUrls(SimpleTestCase):

    def test_bot_endpoint_resolves(self):
        url = reverse('bot_endpoint')
        self.assertEquals(resolve(url).view_name, 'bot_endpoint')

    
