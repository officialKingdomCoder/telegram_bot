from django.test import TestCase, Client
from django.urls import reverse
import json


# Create your tests here.
class TestBotView(TestCase):
    def setUp(self):
        self.client = Client()
        self.endpoint_url = reverse('bot_endpoint')

        self.message_from_outside_group_chat = {
            "update_id": 291128038,
            "message": {
                "message_id": 99,
                "from": {
                    "id": 982774003,
                    "is_bot": False,
                    "first_name": "Oreoluwa",
                    "last_name": "James",
                    "language_code": "en"
                },
                "chat": {
                    "id": 982774003,
                    "first_name": "Oreoluwa",
                    "last_name": "James",
                    "type": "private"
                },
                "date": 1564650876,
                "text": "Hello"
            }
        }

        self.message_from_group_chat_without_text = {
            "update_id": 291128039,
            "message": {
                "message_id": 86,
                "from": {
                    "id": 982774003,
                    "is_bot": False,
                    "first_name": "Oreoluwa",
                    "last_name": "James",
                    "language_code": "en"
                },
                "chat": {
                    "id": -1001301583551,
                    "title": "Ilanaa Demo",
                    "type": "supergroup"
                },
                "date": 1564651367,
            }
        }

        self.message_from_group_with_text = {
            "update_id": 291128040,
            "message": {
                "message_id": 88,
                "from": {
                    "id": 982774003,
                    "is_bot": False,
                    "first_name": "Oreoluwa",
                    "last_name": "James",
                    "language_code": "en"
                },
                "chat": {
                    "id": -1001301583551,
                    "title": "Ilanaa Demo",
                    "type": "supergroup"
                },
                "date": 1564651454,
                "text": "Test"
            }
        }

    def test_get_invalid(self):
        response = self.client.get(self.endpoint_url)

        self.assertEquals(response.status_code, 405)

    def test_outside_group_chat(self):
        response = self.client.post(self.endpoint_url, json.dumps(self.message_from_outside_group_chat), content_type="application/json")

        self.assertEquals(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {"ok": "Invalid Chat"}
        )


    def test_group_no_text(self):
        response = self.client.post(self.endpoint_url, json.dumps(self.message_from_group_chat_without_text), content_type="application/json")

        self.assertEquals(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {"ok": "No Text"}
        )

    def test_group_text(self):
        response = self.client.post(self.endpoint_url, json.dumps(self.message_from_group_with_text), content_type="application/json")

        self.assertEquals(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {"ok": "Message Received"}
        )
