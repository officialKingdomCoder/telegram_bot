from django.views import View
import re as regex
import requests
from bot.models import Message
from dashboard.models import Book
from message_parser.main import extract_message_fields

from django.core import serializers

from bot.setup import BOT_URL


class AugementedView(View):
    @staticmethod
    def save_message(chat_id, message_id, sender_id, text, date):
        """
        Save the message received to the database
        """
        try:
            message = Message(
                chat_id=chat_id,
                message_id=message_id,
                sender_id=sender_id,
                text=text,
                date=date
            )

            message.save()
        except:
            print("Error saving message to database")

    @staticmethod
    def send_response_message(chat_id, return_text):
        """
        Send a response text to the user on telegram
        """

        json_response = {
            "chat_id": chat_id,
            "text": return_text,
        }

        message_url = BOT_URL + 'sendMessage'
        requests.post(message_url, json=json_response)

    @staticmethod
    def generate_message_detail_string(fields):
        response = ""
        for field in fields:
            response += "{key}: {value}\n".format(key=field, value=fields[field])
        return response

    @staticmethod
    def extract_text_fields(text):
        # parse message text to extract patterns
        text = text.lower()
        fields = extract_message_fields(text)

        return fields


def find_books(fields):
    db_table_name = "dashboard_book"
    full_query = "SELECT * FROM {table} WHERE ".format(table=db_table_name)

    conditional_query = ""
    for field in fields:
        if field != "bot_name":
            conditional_query += "AND LOWER({column}) LIKE LOWER('%%{pattern}%%') ".format(column=field, pattern=fields[field])

    conditional_query = conditional_query.lstrip("AND ")
    conditional_query = conditional_query.rstrip(" ")

    full_query += conditional_query + ";"

    print(full_query)

    query_results_object = Book.objects.raw(full_query)

    list_of_result_dictionaries = serializers.serialize("python", query_results_object)

    return list_of_result_dictionaries
