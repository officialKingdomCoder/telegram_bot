from django.contrib import admin
from dashboard.models import Book


class BookAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'author', 'subject')
    list_filter = ('author', 'subject',)
    search_fields = ['title', 'author', 'subject']


admin.site.register(Book, BookAdmin)
