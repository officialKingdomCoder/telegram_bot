from django.urls import path, include
from dashboard.views import BookListView, BookCreateView


urlpatterns = [
    path('', include('django.contrib.auth.urls')),
    path('books/', BookListView.as_view(), name='books'),
    path('new/', BookCreateView.as_view(), name='upload_book')
]
