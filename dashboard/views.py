from django.views.generic import ListView, CreateView
from dashboard.models import Book
from django.urls import reverse_lazy


class BookListView(ListView):
    model = Book
    template_name = 'dashboard/templates/books.html'
    paginate_by = 20
    ordering = ['title']


class BookCreateView(CreateView):
    model = Book
    template_name = 'dashboard/templates/upload_book.html'
    fields = ['title', 'author', 'subject', 'file']
    success_url = reverse_lazy('books')
