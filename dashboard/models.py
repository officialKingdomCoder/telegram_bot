from django.db import models
from django.utils.text import slugify


class Book(models.Model):
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    subject = models.CharField(max_length=100)

    # this function has to be defined before it is refrenced in FileField
    def author_title_path(self, filename):
        # filename is the original name of the book file
        return "{author}/{filename}".format(author=slugify(self.author), filename=filename)

    file = models.FileField(upload_to=author_title_path)
