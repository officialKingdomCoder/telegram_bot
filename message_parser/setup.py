"""
All setings for how tokens are to be extracted from a message
is done here
"""

DEFAULT_TEXT_SPLITTING_PATTERN = r"[^a-zA-Z0-9'\"_-]"

ACCEPT_QUOTATION_SYMBOLS = True
QUOTATION_PATTERN = '"[^"]+"|\'[^\']+\''


PATTERN_FOR_FIELDS_IN_TEXT = r"[a-zA-Z0-9_\- ]+"
PATTERN_FOR_FIELDS_IN_TEMPLATES = "{" + PATTERN_FOR_FIELDS_IN_TEXT + "}"

TEMPLATE_SPLIT_PATTERN = r"[^a-zA-Z0-9{}_-]"

ACCEPTABLE_MESSAGE_PATTERNS = [
                                  "{bot_name}, please, I need {title}",
                                  "{bot_name}, please, I need {title} by {author}",
                                  "{bot_name}, please, I need books by {author}",
                                  "{bot_name}, please, I need books by {author} on {subject}",
                                  "{bot_name}, please, I need books on {subject}"
                                 ]
