from message_parser.classes import State, FieldState, Sequence
import re as regex

from message_parser.setup import PATTERN_FOR_FIELDS_IN_TEXT as text_field_pattern
from message_parser.setup import PATTERN_FOR_FIELDS_IN_TEMPLATES as template_field_pattern
from message_parser.setup import TEMPLATE_SPLIT_PATTERN as template_split_pattern


from message_parser.setup import ACCEPT_QUOTATION_SYMBOLS
if ACCEPT_QUOTATION_SYMBOLS:
    text_field_pattern = "{0}|\"{0}\"|'{0}'".format(text_field_pattern)


def strip(string, list_of_chars):
    for char in list_of_chars:
        string = string.strip(char)
    return string


def strictify(string):
    return r"^{}$".format(string)


def create_sequence(request_template):
    """
        Method to create sequence object from texts of the form:
            "words not in curly brace count as state objects"
            "{Words} in curly braces are FieldState objects"
            "CapITalizaTion is HANDLED. {Punctuations} als0!."
    """
    # field pattern is the pattern for a field palceholder in the
    # boilerplate requests template used to create sequence

    sequence = Sequence()

    request_template = request_template.lower()
    tokens = regex.split(template_split_pattern, request_template)

    for token in tokens:
        if token:
            result = regex.match(template_field_pattern, token)

            if (result):
                # text_field_pattern is the pattern for the expected string
                # gotten from the message that is for a field
                # I'm allowing for quotes in the case of a multi-word field
                sequence.append(FieldState(text_field_pattern, strip(result.group(), ["{", "}"])))
            else:
                sequence.append(State(strictify(token)))

    return sequence
