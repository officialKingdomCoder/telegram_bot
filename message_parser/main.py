from message_parser.field_extractor import FieldExtractor
from message_parser.automaton_generator import AutomatonGenerator
from message_parser.setup import ACCEPTABLE_MESSAGE_PATTERNS as request_templates


def extract_message_fields(message_text):
    ag = AutomatonGenerator(request_templates)
    automaton = ag.generate_automaton()


    fe = FieldExtractor(message_text, automaton)
    fields = fe.extract_fields()
    return fields
