from message_parser.classes import State, FieldState, Sequence, Switch
from message_parser.sequence_creator import create_sequence


class AutomatonGenerator:

    def __init__(self, requests):
        self.requests = requests

    def generate_automaton(self):
        sequences = []
        for request in self.requests:
            sequences.append(create_sequence(request))

        automaton = self.merge_sequences(sequences)
        return automaton

    @classmethod
    def merge_sequences(cls, sequences):
        merged_sequence = Sequence()
        max_sequence_length = max([len(sequence) for sequence in sequences])

        for col_index in range(max_sequence_length):
            states = [row[col_index] for row in sequences if col_index < len(row)]

            if states:
                equal = cls.state_list_elements_equal(states)
                if (equal):
                    merged_sequence.append(states[0])
                else:
                    switch = Switch()
                    groups = cls.group_by_state([row[col_index:] for row in sequences])

                    for group in groups:
                        result = cls.merge_sequences(groups[group])
                        switch[group] = result

                    merged_sequence.append(switch)
                    break
        return merged_sequence

    @classmethod
    def state_list_elements_equal(cls, states):
        if len(states) < 2:
            return True
        else:
            max = range(len(states) - 1)
            for i in max:
                if type(states[i]) is not type(states[i + 1]):
                    return False

                # child class FieldState checked before parent class State
                if isinstance(states[i], FieldState):
                    if states[i].pattern != states[i + 1].pattern:
                        return False
                    if states[i].field != states[i + 1].field:
                        return False
                elif isinstance(states[i], State):
                    if states[i].pattern != states[i + 1].pattern:
                        return False
                else:
                    return False
        return True

    @classmethod
    def group_by_state(cls, sub_sequences):
        groups = {}
        if sub_sequences:
            first_column = [row[0] for row in sub_sequences if row]

            distinct_states = cls.find_distinct_states(first_column)
            # first find the existing groups and add them to the groups
            for state in distinct_states:
                # groups[state] = Sequence()
                groups[state] = []

            for sequence in sub_sequences:
                # check length
                if sequence:
                    for distinct_state in distinct_states:
                        if cls.state_list_elements_equal([sequence[0], distinct_state]):
                            groups[distinct_state].append(sequence)
                            break
        return groups

    @classmethod
    def find_distinct_states(cls, states):
        distinct_states = []
        for state in states:
            new_state = True
            for distinct_state in distinct_states:
                if cls.state_list_elements_equal([state, distinct_state]):
                    new_state = False
                    break
            if new_state:
                distinct_states.append(state)
        return distinct_states
