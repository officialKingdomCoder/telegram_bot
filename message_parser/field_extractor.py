import re as regex
from message_parser.classes import Switch, FieldState

from message_parser.setup import DEFAULT_TEXT_SPLITTING_PATTERN, ACCEPT_QUOTATION_SYMBOLS, QUOTATION_PATTERN


class FieldExtractor:
    def __init__(self, text, sequence, split_pattern=DEFAULT_TEXT_SPLITTING_PATTERN):
        self.sequence = sequence
        self.split_pattern = split_pattern
        self.tokens = self.tokenize(text, split_pattern, ACCEPT_QUOTATION_SYMBOLS)

    def extract_fields(self, sequence=None, tokens=None, fields={}):
        """
            Extract the useful fields from the tokens a given body of text.
            The useful fields are identified using the FieldState objects in the
            sequence passed.
        """

        if sequence is None:
            sequence = self.sequence
        if tokens is None:
            tokens = self.tokens


        # for each element in the sequence
        for i in range(len(sequence)):
            # if there are no more tokens to read
            # This is to handle the where the switch is supposed
            # to allow the case where nothing more is to be read.
            # e.g
                # "books by {author}"
                # vs
                # "books by {author} on subject"
            # the is statement is to allow for handling the case:
                # "books by {author}"
            # by a seqence designed to handle
                # "books by {author} on subject"
            # also.
            if(i >= len(tokens)):
                return fields
            # otherwise
            else:
                # if the element is a (child class of) state (i.e not switch)
                if not isinstance(sequence[i], Switch):
                    # if the current sequence element representrs a field
                    if isinstance(sequence[i], FieldState):
                        # check if text token matches the field pattern
                        result = regex.match(sequence[i].pattern, tokens[i])
                        if (result):
                            field_value = result.group()
                            fields[sequence[i].field] = field_value


                        # if the token is in a position where a field is
                        # expected but it does not match the field pattern,
                        # it is wrong
                        else:
                            return fields

                    # In the case that it is not a field, the token must
                    # still match the  pattern expected  in the position
                    # it is
                    elif not (regex.match(sequence[i].pattern, tokens[i])):
                        return fields

                # however, if it is a switch
                else:

                    for key in sequence[i]:
                        result = regex.match(key.pattern, tokens[i])
                        if (result):
                            response = self.extract_fields(sequence[i][key], tokens[i:], fields)
                            if response:
                                fields.update(response)
                    return fields
        return fields

    @classmethod
    def tokenize(cls, text, split_pattern, accept_quotation_symbols):
        text = text.lower()
        if accept_quotation_symbols:
            quotations = regex.findall(QUOTATION_PATTERN, text)

            # ensure correct behaviour of findall method
            if quotations:
                splits = regex.split(QUOTATION_PATTERN, text)

                tokens = []
                quotation_match_index = 0
                for token in splits:
                    # if token == "" means a quote is supposed to be here
                    if token == "":
                        quotation = quotations[quotation_match_index]
                        quotation = quotation.strip("'")
                        quotation = quotation.strip('"')
                        tokens.append(quotation)
                        quotation_match_index += 1
                        # there is no need to verify index length becaus the
                        # number of findall tokens should be equal to the
                        # number of "" in split

                    else:
                        substring_splits = cls.tokenize(token, split_pattern, accept_quotation_symbols)
                        tokens.extend(substring_splits)
                return tokens
            else:
                tokens = cls.tokenize(text, split_pattern, accept_quotation_symbols=False)
                return tokens

        else:

            tokens = regex.split(split_pattern, text)
            tokens = [token for token in tokens if token]
            return tokens
